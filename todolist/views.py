from contextlib import contextmanager
from .model import ToDo
import falcon
import json


@contextmanager
def create_session(session_maker):
    session = session_maker()
    try:
        yield session
        session.commit()

    except:
        session.rollback()
        raise

    finally:
        session.close()


class TodoList(object):
    def __init__(self, session_maker):
        self._session_maker = session_maker

    def on_get(self, req, resp):
        with create_session(self._session_maker) as session:
            todo_list = session.query(ToDo).all()
            values = [todo.dict for todo in todo_list]
            resp.body = json.dumps({"todo_list": values})  # avoid top level array vulnerability

    def on_post(self, req, resp):
        pass  # todo: cannot resist a pun


class TodoItem(object):
    def __init__(self, session_maker):
        self._session_maker = session_maker

    def on_get(self, req, resp, item_id):
        with create_session(self._session_maker) as session:
            todo = session.query(ToDo).filter_by(id=item_id).first()
            if todo is None:
                raise falcon.HTTPNotFound()

            result = todo.dict
            result["description"] = todo.description
            resp.body = json.dumps(result)