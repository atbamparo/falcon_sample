import falcon
from todolist.views import TodoList, TodoItem
from sqlalchemy import engine_from_config
from sqlalchemy.orm import sessionmaker
from .model import Base, ToDo


def make_wsgi_app(**settings):
    engine = engine_from_config(settings)
    session_maker = sessionmaker()
    session_maker.configure(bind=engine)

    api = falcon.API()
    api.add_route("/todo/{item_id}", TodoItem(session_maker))
    api.add_route("/todo", TodoList(session_maker))
    return api
