from collections import OrderedDict
from sqlalchemy import Column, Integer, String, Text
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class ToDo(Base):
    def __init__(self, name, description):
        self.name = name
        self.description = description

    @property
    def dict(self):  # do not use __dict__ property here, it crashes the interpreter by infinite recursion
        result = OrderedDict()
        result['id'] = self.id
        result['name'] = self.name
        return result

    __tablename__ = "to_do"
    id = Column("id", Integer, primary_key=True, autoincrement=True)
    name = Column("name", String, nullable=False)
    description = Column("description", Text, nullable=True)