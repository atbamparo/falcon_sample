import waitress
import todolist

if __name__ == "__main__":
    settings = {'sqlalchemy.url': "sqlite:///database.sqlite3"}
    app = todolist.make_wsgi_app(**settings)
    waitress.serve(app, host="0.0.0.0", port=4321)